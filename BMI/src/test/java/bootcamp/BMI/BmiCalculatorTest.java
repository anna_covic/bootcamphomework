package bootcamp.BMI;

import junit.framework.TestCase;

public class BmiCalculatorTest extends TestCase {
    
    private BmiCalculator bmitest;

	

    public void setUp() throws Exception {
	bmitest = new BmiCalculator(60, 1.69);
	
    }
    

    public void testPossibleNumbersShouldGiveBMIandCategory() {
	
	double goodResult = Math.round(60 / Math.pow(1.69, 2));
	
	double result = bmitest.getBMI();
	
	assertEquals("Calculation error.", goodResult, result, 0.1);
	
	String category = bmitest.BMICategory();
	
	assertEquals("Wrong category.", "Normal (healthy weight)", category);
	
	
    }

}
