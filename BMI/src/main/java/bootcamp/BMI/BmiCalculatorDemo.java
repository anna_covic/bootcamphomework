package bootcamp.BMI;

public class BmiCalculatorDemo {

    public static void main(String[] args) {
	
	double weight = 57;
	double height = 1.66;
	

	try {
	
	BmiCalculator person = new BmiCalculator(weight, height);
		
	System.out.println("The BMI is: " + person.getBMI());
	System.out.println("\nThis BMI falls into the category: " + person.BMICategory());
	
	}
	
	catch (IllegalArgumentException excp) {
	    System.out.println(excp);
	}

    }

}
