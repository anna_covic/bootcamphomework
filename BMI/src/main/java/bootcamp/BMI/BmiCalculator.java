package bootcamp.BMI;

/**
 * A small program to calculate Body Mass Index (BMI).
 * 
 * The program's using the International System of Units (SI).
 *
 */

public class BmiCalculator {
    
    public double weightInKg, heightInMeter, bmi;
    
    public BmiCalculator(double weight, double height) throws IllegalArgumentException {
	
	if (weight <= 0)
	    throw new IllegalArgumentException("Weight can not be a negative number or 0.");
	if (weight > 700)
	    throw new IllegalArgumentException("Weight value is too big.");
	if (height <= 0)
	    throw new IllegalArgumentException("Height can not be a negative number or 0. ");
	if (height > 3.0)
	    throw new IllegalArgumentException("Hight value is too big.");
	
	weightInKg = weight;
	heightInMeter = height;
    }

    /**
     * Calculate and return BMI.
     * 
     * @return
     */
    
    public double getBMI() {
	bmi = weightInKg / Math.pow(heightInMeter, 2);
	return Math.round(bmi);
    }
    
    /**
     * Get and return BMI categories.
     * 
     * @return
     */
    
    
    public String BMICategory() {
	
	double bmi = getBMI();
	
	if (bmi < 16)
	    return "Very severely underweight";
	else if (bmi < 17)
	    return "Severely underweight";
	else if (bmi < 19)
	    return "Underweight";
	else if (bmi < 26)
	    return "Normal (healthy weight)";
	else if (bmi < 31)
	    return "Overweight";
	else if (bmi < 36)
	    return "Obese Class I (Moderately obese)";
	else if (bmi < 41)
	    return "Obese Class II (Severely obese)";
	else
	    return "Obese Class III (Very severely obese)";
    }
    
}



